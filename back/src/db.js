import sqlite from "sqlite";
import SQL from "sql-template-strings";
const initializeDatabase = async () => {
  const db = await sqlite.open("./db.sqlite");

  //     await db.run(`CREATE TABLE burshes (burshes_id INTEGER PRIMARY KEY AUTOINCREMENT, hair TEXT NOT NULL, width INTEGER NOT NULL ,size TEXT NOT NULL );`);
  //     const stmt = await db.prepare(SQL`INSERT INTO burshes (hair, size,width) VALUES (?, ?,?)`);
  //     let i = 0;
  //     while(i<10){
  //       await stmt.run("Cat",`${i}`,"Small");
  //       i++
  //     }
  //     await stmt.finalize();
  //     const rows = await db.all("SELECT burshes_id AS id, hair, width ,size FROM burshes")
  //     rows.forEach( ({ id, hair, width,size }) => console.log(`[id:${id}] - ${hair} - ${width} -${size}`) )
  //   }

  //   export default { test }
  const getBurshesList = async () => {
    const rows = await db.all(
      "SELECT burshes_id AS id, hair, width ,size FROM burshes"
    );
    return rows;
  };

  // const getBurshesList = async () => {
  //     let returnString = ""
  //     const rows = await db.all("SELECT burshes_id AS id, hair, width ,size FROM burshes")
  //     rows.forEach( ({ id, hair, width,size }) => returnString+=`[id:${id}] - ${hair} - ${width} -${size}`)
  //    // console.log(returnString)
  //     return returnString
  //   }
  const createBurshes = async props => {
    if(!props || !props.hair || !props.width || !props.size){
        throw new Error(`you must provide a name and an email`)
      }
  
    const { hair, width, size } = props;
    try{
    const result = await db.run(
      SQL`INSERT INTO burshes (hair,width,size) VALUES (${hair}, ${width} ,${size})`
    );
    const id = result.stmt.lastID;
    return id;}
    catch(e){
        throw new Error(`couldn't insert this combination: `+e.message)
    }
  };
  //delete
  const deleteBurshes = async id => {
    try{
        const result = await db.run(SQL`DELETE FROM burshes WHERE burshes_id= ${id}`);
        if(result.stmt.changes === 0){
          throw new Error(`contact "${id}" does not exist`)
        }
        return true
      }catch(e){
        throw new Error(`couldn't delete the contact "${id}": `+e.message)
      }
  };
  //update
  const updateBurshes = async (id, props) => {
    const { hair, width, size } = props;
    const result = await db.run(SQL`UPDATE burshes SET hair=${hair} WHERE burshes_id = ${id}`);
    if(result.stmt.changes === 0){
      return false
    }
    return true
  }
  const getBurshes = async (id) => {
    try{
        const burshesList = await db.all(SQL`SELECT burshes_id AS id, hair, width,size FROM burshes WHERE burshes_id = ${id}`);
        const bursh = burshesList[0]
        if(!bursh){
          throw new Error(`bursh ${id} not found`)
        }
        return bursh
      }catch(e){
        throw new Error(`couldn't get the bursh ${id}: `+e.message)
      }
  }
  const getBursheList = async (orderBy) => {
    try{
      
        let statement = `SELECT burshes_id AS id, hair, width ,size FROM burshes`
        switch(orderBy){
            case 'hair': statement+= ` ORDER BY hair`; break;
            case 'size': statement+= ` ORDER BY size`; break;
            default: break;
        }
        const rows = await db.all(statement)
        if(!rows.length){
          throw new Error(`no rows found`)
        }
        return rows
      }catch(e){
        throw new Error(`couldn't retrieve contacts: `+e.message)
      }
  }




  const controller = {
    getBurshesList,
    createBurshes,
    deleteBurshes,
    updateBurshes,
    getBursheList,
    getBurshes
  };

  return controller;
};

export default initializeDatabase;
