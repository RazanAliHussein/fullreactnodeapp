// import { createServer } from 'http'

// const whenRequestReceived = (request /* the request sent by the client*/, response /* the response we use to answer*/) => {

//     response.writeHead(200, { 'Content-type': `text/plain` });

//     response.write(`Hello`);

//     response.end( );
//   }

//   const server = createServer(whenRequestReceived)
//   server.listen(8080, ()=>{console.log('ok, listening')});
//22
// import app from './app'

// app.get( '/', (req, res) => res.send("ok") );

// app.listen( 8080, () => console.log('server listening on port 8080') )
// 33
// import app from './app'
// import initializeDatabase from './db'

// const start = async () => {
//   const controller = await initializeDatabase()
//   const contacts_list = await controller.getContactsList()
//   console.log(contacts_list)
//   app.get('/',(req,res)=>res.send("ok"));
//   app.listen(8080, ()=>console.log('server listening on port 8080'))
// }

// start();
import app from "./app";
import initializeDatabase from "./db";
import { authenticateUser, logout, isLoggedIn } from "./auth";
const start = async () => {
  // const controller = await initializeDatabase()
  // const id = await controller.createBurshes({hair:"horse",width:"5",size:"Big"})
  // const contact = await controller.getBurshes(id)
  // console.log("------\nmy newly created contact\n",contact)

  const controller = await initializeDatabase();

  app.get("/", (req, res) => res.send("ok"));
  //auth
  app.get("/login", authenticateUser);

  app.get("/logout", logout);

  app.get("/mypage", isLoggedIn, (req, res) => {
    const username = req.user.name;
    res.send({
      success: true,
      result: "ok, user " + username + " has access to this page"
    });
  });

  //auth
  app.get("/brushes/list", async (req, res) => {
    try {
      const burshes_list = await controller.getBurshesList();
      res.json(burshes_list);
    } catch (e) {
      next(e);
    }
  });
  // CREATE
  app.get("/brushes/new", async (req, res, next) => {
    try {
      const { hair, width, size } = req.query;
      const result = await controller.createBurshes({ hair, width, size });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  // READ
  app.get("/brushes/get/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const brush = await controller.getBurshes(id);
      res.json({ success: true, result: brush });
    } catch (e) {
      next(e);
    }
  });
  // DELETE
  app.get("/brushes/delete/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const result = await controller.deleteBurshes(id);
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  // UPDATE
  app.get("/brushes/update/:id", async (req, res, next) => {
    try {
      const { id } = req.params;
      const { hair, width, size } = req.query;
      const result = await controller.updateBurshes(id, { hair, width, size });
      res.json({ success: true, result });
    } catch (e) {
      next(e);
    }
  });
  // LIST
  app.get("/brushes/list", async (req, res, next) => {
    try {
      const { order } = req.query;
      const contacts = await controller.getBursheList(order);
      res.json({ success: true, result: contacts });
    } catch (e) {
      next(e);
    }
  });

  app.use((err, req, res, next) => {
    try {
      console.error(err);
      const message = err.message;
      res.status(500).json({ success: false, message });
    } catch (e) {
      next(e);
    }
  });

  app.listen(8080, () => console.log("server listening on port 8080"));
};
start();
